package Homework09;
import static java.lang.Math.PI;

public class Circle extends Ellipse{

    public Circle(double radius1){
        super(radius1, radius1);
    }

    public double getPerimeter(){
        return 2.0*PI*radius1;
    }
}
