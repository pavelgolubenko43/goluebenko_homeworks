package Homework09;

public class Square extends Rectangle {

    public Square(double length){
        super(length, length);
    }

    public double getPerimeter(){
        return 4*length;
            }
}

