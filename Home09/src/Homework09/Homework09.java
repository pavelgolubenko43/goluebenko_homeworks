package Homework09;

public class Homework09 {

    public static void main(String[] args) {

    Figure figure = new Figure(0,0);
    Ellipse ellipse = new Ellipse(9, 12);
    Circle circle = new Circle(8);
    Rectangle rectangle = new Rectangle(5, 6);
    Square square = new Square(14);


        System.out.println(figure.getPerimeter());
        System.out.println(ellipse.getPerimeter());
        System.out.println(circle.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(square.getPerimeter());
    }
}

