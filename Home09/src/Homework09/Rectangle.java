package Homework09;

public class Rectangle extends Figure {
    protected double length;
    protected double width;

    public Rectangle(double length, double width){
        this.length = length;
        this.width = width;
    }
    public double getPerimeter(){
        return 2.0*length+2.0*width;
    }
}
